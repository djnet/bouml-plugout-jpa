BOUML_DIR=/usr/lib/bouml
RELEASE_DIR=.

# copy jar, launcher
cp $RELEASE_DIR/plugout-jpa.jar $BOUML_DIR
cp $RELEASE_DIR/plugout-jpa $BOUML_DIR

# copy project template
cp -r $RELEASE_DIR/jpa-template $BOUML_DIR

# copy test project 
cp -r $RELEASE_DIR/jpa-project-test $BOUML_DIR
