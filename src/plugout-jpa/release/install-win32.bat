set BOUML_DIR=C:\Program Files\Bouml
set RELEASE_DIR=.

REM copy jar, launcher
cp "%RELEASE_DIR%\plugout-jpa.jar" "%BOUML_DIR%"
cp "%RELEASE_DIR%\plugout-jpa.exe" "%BOUML_DIR%\plugout-jpa.exe"

REM copy project template
cp -r "%RELEASE_DIR%\jpa-template" "%BOUML_DIR%"

REM copy test project 
cp -r "%RELEASE_DIR%\jpa-project-test" "%BOUML_DIR%"
